/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jandoja4.smarthouse;

import com.jandoja4.smarthouse.enums.DeviceState;

/**
 *
 * @author jarda
 */
public class DeviceObserver {
    
    Integer totalEnergyConsumption;
    Integer totalWaterConsumption;

   

    

    public Integer getTotalEnergyConsumption() {
        return totalEnergyConsumption;
    }

    public void setTotalEnergyConsumption(Integer totalEnergyConsumption) {
        this.totalEnergyConsumption += totalEnergyConsumption;
    }

    public Integer getTotalWaterConsumption() {
        return totalWaterConsumption;
    }

    public void setTotalWaterConsumption(Integer totalWaterConsumption) {
        this.totalWaterConsumption += totalWaterConsumption;
    }
    
    
    
    
    
}
