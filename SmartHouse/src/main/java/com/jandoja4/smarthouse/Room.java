package com.jandoja4.smarthouse;

import java.util.ArrayList;
import java.util.Vector;

public class Room {

  public Integer squareFootage;
  public String nameOfRoom;

    public Floor isOnFloor;
    /**
   * 
   * @element-type Window
   */
  public ArrayList<Window>  windowsInRoom;
    /**
   * 
   * @element-type Sensor
   */
  public ArrayList<Sensor>  sensorsInRoom;
    /**
   * 
   * @element-type Device
   */
  public ArrayList<Device>  devicesInRoom;

    /**
   * 
   * @element-type Being
   */
  public ArrayList<Being>  beingsPresentInRoom;

  public Room(int squareFootage,String nameOfRoom) {
      this.squareFootage=squareFootage;
      this.nameOfRoom=nameOfRoom;
    }
  
  
  

}