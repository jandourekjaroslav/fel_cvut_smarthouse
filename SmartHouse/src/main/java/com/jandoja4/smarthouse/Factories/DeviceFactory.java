/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jandoja4.smarthouse.Factories;

import com.jandoja4.smarthouse.APIs.DeviceApi;
import com.jandoja4.smarthouse.Device;
import com.jandoja4.smarthouse.enums.DeviceState;
import com.jandoja4.smarthouse.Room;
import com.jandoja4.smarthouse.SpecialUtilityDevice;
import com.jandoja4.smarthouse.enums.SpecialUtilityDeviceType;

/**
 *
 * @author jarda
 */
public class DeviceFactory {
    int deviceId=0;
    DeviceApi deviceApi=new DeviceApi();
    
    
    
public DeviceApi createDevice(Room room,Integer idleEnergyConsumption, Integer onEnergyConsumption, Integer offEnergyConsumption,String nameOfDevice,Integer onWaterConsumption){
    Device newDevice =new Device(nameOfDevice, idleEnergyConsumption, onEnergyConsumption, offEnergyConsumption, room,onEnergyConsumption);
    newDevice.isInState=DeviceState.Off;
    newDevice.deviceId=deviceId;
    System.out.println("Added device with name"+ nameOfDevice +"and assigned it ID:"+deviceId);
    deviceId++;

    room.devicesInRoom.add(newDevice);
    deviceApi.InitializeDevice(newDevice);
    
    return deviceApi;


}    
public DeviceApi createSpecialUtilityDevice(Room room,Integer idleEnergyConsumption, Integer onEnergyConsumption, Integer offEnergyConsumption,String nameOfDevice,SpecialUtilityDeviceType type,Integer onWaterConsumption){
    SpecialUtilityDevice newDevice =new SpecialUtilityDevice(type, nameOfDevice, idleEnergyConsumption, onEnergyConsumption, offEnergyConsumption, room,onWaterConsumption);
    newDevice.isInState=DeviceState.Off;
    newDevice.deviceId=deviceId;
    System.out.println("Added device with name"+ nameOfDevice +"and assigned it ID:"+deviceId);
    deviceId++;

    room.devicesInRoom.add(newDevice);
    
    deviceApi.InitializeDevice(newDevice);
    
    return deviceApi;

}
}    

