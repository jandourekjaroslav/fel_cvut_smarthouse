/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jandoja4.smarthouse.Factories;

import com.jandoja4.smarthouse.APIs.ItemApi;
import com.jandoja4.smarthouse.Item;
import com.jandoja4.smarthouse.NonParishable;
import com.jandoja4.smarthouse.Parishable;
import com.jandoja4.smarthouse.Person;
import java.util.Date;

/**
 *
 * @author jarda
 */
public class ItemFactory {
    ItemApi itemApi=new ItemApi();
    public ItemApi addFreeStandingItem(String itemDescription){
        Item newItem=new Item(itemDescription, true);
        itemApi.initializeItem(newItem);
        return itemApi;
    
    }
    public ItemApi addItemContainableInDevice(String itemDescription, Person owner){
        Item newItem=new Item(itemDescription, false);
        itemApi.initializeItem(newItem);

        return itemApi;
    
    
    }
    public ItemApi addParishableItem(String itemDescription, Date expirationDate){
        Parishable newItem=new Parishable(itemDescription, true,  expirationDate);
        itemApi.initializeItem(newItem);

        return itemApi;

    
    }
    public ItemApi addNonParishableItem(String itemDescription){
         NonParishable newItem=new NonParishable(itemDescription, false);
        itemApi.initializeItem(newItem);
        return itemApi;

    }
    
}
