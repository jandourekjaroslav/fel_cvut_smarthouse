/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jandoja4.smarthouse.Factories;

import com.jandoja4.smarthouse.APIs.SensorApi;
import com.jandoja4.smarthouse.Room;
import com.jandoja4.smarthouse.Sensor;
import com.jandoja4.smarthouse.enums.SensorType;

/**
 *
 * @author jarda
 */
public class SensorFactory {
    SensorApi sensorApi=new SensorApi();
    public SensorApi addSensor(Room room,String unitsOfMeasure, SensorType typeOfMeasurement,Integer criticalLowValue,Integer criticalHighValue){
        Sensor newSensor=new Sensor(unitsOfMeasure, typeOfMeasurement,criticalLowValue,criticalHighValue);
        newSensor.inRoom=room;
        
        room.sensorsInRoom.add(newSensor);
        sensorApi.initializeSensor(newSensor);
        return sensorApi;
    
    
    }
    
}
