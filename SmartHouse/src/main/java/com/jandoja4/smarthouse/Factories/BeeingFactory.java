/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jandoja4.smarthouse.Factories;

import com.jandoja4.smarthouse.House;
import com.jandoja4.smarthouse.Person;
import com.jandoja4.smarthouse.Pet;
import com.jandoja4.smarthouse.Role;

/**
 *
 * @author jarda
 */
public class BeeingFactory {
    final House factoryForHouse;
    
    
    public Person addPerson(Role role,String name,int age,String gender){
       
        
        Person newPerson=new Person(role, name, age, gender, factoryForHouse);
        factoryForHouse.inhabitedBy.add(newPerson);
        newPerson.curentlyIn=factoryForHouse.madeUpOfFloors.get(0).consitsOf.get(0);
        factoryForHouse.madeUpOfFloors.get(0).consitsOf.get(0).beingsPresentInRoom.add(newPerson);

        return newPerson;
    
    }
    public Pet addPet(String name,int age,String gender,String species){
        Pet newPet=new Pet(name, age, gender, species,factoryForHouse);
        factoryForHouse.inhabitedBy.add(newPet);
        return newPet;
       
    
    }
    public BeeingFactory(House factoryFor){
        this.factoryForHouse=factoryFor;
    }
}
