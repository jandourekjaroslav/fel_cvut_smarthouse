/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jandoja4.smarthouse.Factories;

import com.jandoja4.smarthouse.APIs.BlindsApi;
import com.jandoja4.smarthouse.PersonalController;
import com.jandoja4.smarthouse.APIs.DeviceApi;
import com.jandoja4.smarthouse.APIs.ItemApi;
import com.jandoja4.smarthouse.APIs.MeansOfTransportApi;
import com.jandoja4.smarthouse.Person;
import com.jandoja4.smarthouse.APIs.SensorApi;

/**
 *
 * @author jarda
 */
public class ControllerFactory {
    
    
    public void createControllerForPerson( DeviceApi deviceApi, BlindsApi blindsApi, SensorApi sensorApi, ItemApi itemApi, Person Owner,MeansOfTransportApi motApi){
        PersonalController newController=new PersonalController(Owner.inhabiting, deviceApi, blindsApi, sensorApi, itemApi, Owner,motApi);
        Owner.controller=newController;
        
        
        
    }
    
}
