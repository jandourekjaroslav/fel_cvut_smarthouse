/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jandoja4.smarthouse.Factories;

import com.jandoja4.smarthouse.APIs.BlindsApi;
import com.jandoja4.smarthouse.Blinds;
import com.jandoja4.smarthouse.Floor;
import com.jandoja4.smarthouse.House;
import com.jandoja4.smarthouse.Room;
import com.jandoja4.smarthouse.Window;

/**
 *
 * @author jarda
 */
public class HouseFactory {
    BlindsApi blindsApi=new BlindsApi();
    
    public House createHouse(String city,String address,String postCode){
        return new House(city, address, postCode);
    
    }
    public Floor addFloor(House house,int level)throws Error{
    
        Floor newFloor=new Floor(level);
        if(house.madeUpOfFloors.get(level)!=null){
            throw new Error("Floor already exists at this level");
            
        }
        house.madeUpOfFloors.add(level,newFloor);
        newFloor.belongsTo=house;
        return newFloor;
    
    }
    public Room addRoom(Floor onFloor,int squareFootage,String Name){
        Room newRoom=new Room(squareFootage,Name);
        
        onFloor.consitsOf.add(newRoom);
        newRoom.isOnFloor=onFloor;
        
        return newRoom;
    
    
    }
    public Window addWindow(Room inRoom){
        Window newWindow=new Window();
        inRoom.windowsInRoom.add(newWindow);
        newWindow.inRoom=inRoom;
        return newWindow;
    
    
    }
    public BlindsApi addBlinds(Window coverWindow){
        Blinds newBlinds=new Blinds();
        newBlinds.openedToPercentage=0;
        newBlinds.covers=coverWindow;
        coverWindow.coveredBy=newBlinds;
        blindsApi.initializeBlinds(newBlinds);
        return blindsApi;
    
    
    }
    
}
