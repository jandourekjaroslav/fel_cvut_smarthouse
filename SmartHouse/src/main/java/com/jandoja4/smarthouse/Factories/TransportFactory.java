/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jandoja4.smarthouse.Factories;

import com.jandoja4.smarthouse.APIs.MeansOfTransportApi;
import com.jandoja4.smarthouse.Bicycle;
import com.jandoja4.smarthouse.Car;
import com.jandoja4.smarthouse.MeansOfTransport;
import com.jandoja4.smarthouse.Person;

/**
 *
 * @author jarda
 */
public class TransportFactory {
    MeansOfTransportApi motApi=new MeansOfTransportApi();
    
    public MeansOfTransportApi createGenericMeansOfTransportation(Integer manufacturedIn, String condition, String description ){
        MeansOfTransport newTrans=new MeansOfTransport(manufacturedIn, condition, description );
                motApi.initializeMoT(newTrans);

        return motApi;
    }
    public MeansOfTransportApi createCar(Integer manufacturedIn, String condition, String description ,Integer milage,Integer fuelInTank){
        Car newTrans=new Car(manufacturedIn, condition, description , milage, fuelInTank);
                motApi.initializeMoT(newTrans);

        return motApi;
    
    }
    public MeansOfTransportApi createBicycle(Integer manufacturedIn, String condition, String description ){
        Bicycle newTrans=new Bicycle(manufacturedIn, condition, description );
        motApi.initializeMoT(newTrans);
        return motApi;
    
    }
}
