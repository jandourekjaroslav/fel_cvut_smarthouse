package com.jandoja4.smarthouse;

import java.util.Date;

public class Parishable extends Item {

  public Date expirationDate;

    public Parishable(String itemDescription, boolean freeStanding,Date exDate) {
        super(itemDescription, freeStanding);
        this.expirationDate=exDate;
    }

}