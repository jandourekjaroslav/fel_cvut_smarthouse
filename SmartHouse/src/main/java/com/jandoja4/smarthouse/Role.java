/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jandoja4.smarthouse;

/**
 *
 * @author jarda
 */
public enum Role{
    HeadOfFamily,
    Parent,
    GrandParent,
    Child
};
