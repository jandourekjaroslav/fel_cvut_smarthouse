/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jandoja4.smarthouse.APIs;

import com.jandoja4.smarthouse.*;


import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author jarda
 */
public class ItemApi {
    ArrayList<Item> itemsInControll=new ArrayList<>();
    ArrayList<Parishable> parishablesInControll=new ArrayList<>();
    
    public void initializeItem(Item item){
        itemsInControll.add(item);
    
    }
    public void initializeItem(Item[] items){
        for(Item a:items){
            itemsInControll.add(a);
        
        }
    
    }
    public void initializeItem(Parishable item){
       parishablesInControll.add(item);
    
    }
    public void initializeItem(Parishable[] items){
        for(Parishable a:items){
            parishablesInControll.add(a);
        
        }
    
    }
    public Date checkSellByDate(Parishable parishable){
        return parishable.expirationDate;
    
    }
    public Parishable[] findAllItemsPastSellByDate(){
        ArrayList<Parishable>returnArrayList=new ArrayList<>();
        for(Parishable a:parishablesInControll){
           
            if(a.expirationDate.after(Calendar.getInstance().getTime())){
                returnArrayList.add(a);
            
            
            }
        
        }
        
    
    
        return (Parishable[]) returnArrayList.toArray();
    }
    public Item[] findItemByDescription(String description){
            ArrayList<Item> returnList=new ArrayList<>();
            for(Item a:itemsInControll){
                if(a.itemDescription.contains(description)){
                    returnList.add(a);
                
                }
            
            }
        
            return (Item[]) returnList.toArray();
    }
    public boolean personUseItem(Person person,Item item){
        if(item.usedBy==null){
             item.usedBy=person;
             return true;
        }
        return false;
    }
    public void stopUsingItem(Item itm){
        itm.usedBy=null;
    
    }

}

