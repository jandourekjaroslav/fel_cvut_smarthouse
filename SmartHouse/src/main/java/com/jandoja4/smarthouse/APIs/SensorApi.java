/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jandoja4.smarthouse.APIs;

import com.jandoja4.smarthouse.enums.DeviceState;
import com.jandoja4.smarthouse.Sensor;
import com.jandoja4.smarthouse.SpecialUtilityDevice;
import com.jandoja4.smarthouse.enums.SpecialUtilityDeviceType;
import java.util.ArrayList;

/**
 *
 * @author jarda
 */
public class SensorApi {
    ArrayList<Sensor> conntrolledSensors=new ArrayList<>();
    DeviceApi deviceApi;
    BlindsApi blindsApi;

   public void initializeSensorApi(DeviceApi deviceApi,BlindsApi blindsApi){
      this.blindsApi=blindsApi;
      this.deviceApi=deviceApi;
   
   } 
   public void initializeSensor(Sensor sensor){
       conntrolledSensors.add(sensor);
       
   
   
   }
   public void intializeSensor(Sensor[] sensors){
       for(int i=0;i<sensors.length;i++){
           conntrolledSensors.add(sensors[i]);
       
       }
   
   
   }
   public void takeMesurments(){
       for(Sensor a:conntrolledSensors){
           System.out.println("Taking a reading from "+a.typeOfMeasurement.name()+ " sensor in "+ a.inRoom.nameOfRoom);
       
           if(a.measuredValue>=a.criticalHighValue|| a.measuredValue<=a.criticalLowValue){
               reactToCriticalValueReached(a);
           
           }
       }
       //log it whatever
   
   }
   public void reactToCriticalValueReached(Sensor sensor){
       switch(sensor.typeOfMeasurement){
       
           case Light:
               if(sensor.measuredValue>=sensor.criticalHighValue){
                  for(SpecialUtilityDevice a:deviceApi.utilityInControll){
                      if(a.deviceType==SpecialUtilityDeviceType.Light){
                      a.isInState=DeviceState.Off;
                      }
                  
                  }
               
               }else{
                   for(SpecialUtilityDevice a:deviceApi.utilityInControll){
                      if(a.deviceType==SpecialUtilityDeviceType.Light){
                      a.isInState=DeviceState.On;
                      }
                  
                  }
               
               }
               break;
               
           case Smoke:
               if(sensor.measuredValue>=sensor.criticalHighValue){
                   //give alert,call firebrigade
               
               }
               break;
           
           
               
               
           case WindSpeed: 
               if(sensor.measuredValue>=sensor.criticalHighValue){
                   blindsApi.closeAllBlinds();
               
               }
               break;
           case Moisture:
               for(SpecialUtilityDevice a:deviceApi.utilityInControll){
                      if(a.deviceType==SpecialUtilityDeviceType.HumidityControl){
                      a.isInState=DeviceState.On;
                      }
                  
                  }
               break;
               
           case Temperature:
                if(sensor.measuredValue>=sensor.criticalHighValue){
                   for(SpecialUtilityDevice a:deviceApi.utilityInControll){
                      if(a.deviceType==SpecialUtilityDeviceType.Heater){
                      a.isInState=DeviceState.Off;
                      }
                  
                  }
                   for(SpecialUtilityDevice a:deviceApi.utilityInControll){
                      if(a.deviceType==SpecialUtilityDeviceType.ACUNIT){
                      a.isInState=DeviceState.On;
                      }
                  
                  }
               
               }else{
                   if(sensor.measuredValue>=sensor.criticalHighValue){
                        for(SpecialUtilityDevice a:deviceApi.utilityInControll){
                           if(a.deviceType==SpecialUtilityDeviceType.Heater){
                           a.isInState=DeviceState.On;
                           }

                       }
                        for(SpecialUtilityDevice a:deviceApi.utilityInControll){
                           if(a.deviceType==SpecialUtilityDeviceType.ACUNIT){
                           a.isInState=DeviceState.Off;
                           }
                  
                  }
                
                }
       
       
       
       }
   
   
   } 
}
}
