/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jandoja4.smarthouse.APIs;

import com.jandoja4.smarthouse.Device;
import com.jandoja4.smarthouse.enums.DeviceState;
import com.jandoja4.smarthouse.Item;
import com.jandoja4.smarthouse.Person;
import com.jandoja4.smarthouse.SpecialUtilityDevice;
import java.util.ArrayList;

/**
 *
 * @author jarda
 */
public class DeviceApi {
    ArrayList<Device> devicesInControll=new ArrayList<>();
    ArrayList<SpecialUtilityDevice> utilityInControll=new ArrayList<>();


    public DeviceApi() {
       
    }
    
    public void InitializeDevice(Device initDevice){
        devicesInControll.add(initDevice);
        
    
    
    }
    
     public void InitializeDevice(SpecialUtilityDevice initDevice){
        utilityInControll.add(initDevice);
        
    
    
    }
    public void InitializeDevice(SpecialUtilityDevice[] devices){
        for (SpecialUtilityDevice device : devices) {
            utilityInControll.add(device);
        }
    
    
    } 
    public void InitializeDevice(Device[] devices){
        for (Device device : devices) {
            devicesInControll.add(device);
        }
    
    
    }
    
    public Device[] findDeviceByName(String name){
        ArrayList<Device> returnList=new ArrayList<Device>();
        for (Device a:devicesInControll){
            if(a.nameOfDevice == null ? name == null : a.nameOfDevice.equals(name)){
                returnList.add(a);
            }
        
        }
    return (Device[]) returnList.toArray();
    }
    public Device findDeviceById(int id) throws Error{
        for(Device a:devicesInControll){
            if(a.deviceId==id){
                return a;
            
            }
        
        }
    
        throw new Error("Device Not Found");
    }
    public boolean turnDeviceOff(Device device){
        for (Device device1 : devicesInControll) {
            if(device1==device){
                if(device1.isInState==DeviceState.Broken){
                    System.err.println("Device is BROKEN and cannot be interacted with until fixed ");
                     return false;
               
                }
                if(device1.isInState==DeviceState.Off){
                    System.err.println("Device is alreade Off");
                    return false;
               
                }
               
                device1.isInState=DeviceState.Off;
                return true;
            }
        }
            System.err.println("Device not initialized,use factory for device creation ");

        return false;
    }
    public boolean turnDeviceOn(Device device){
        for (Device device1 : devicesInControll) {
            if(device1==device){
                if(device1.isInState==DeviceState.Broken){
                    System.err.println("Device is BROKEN and cannot be interacted with until fixed ");
                     return false;
               
                }
                if(device1.isInState==DeviceState.On){
                    System.err.println("Device is already On ");
                    return false;
                
                }
                device1.isInState=DeviceState.On;
                return true;
            }
        }
            System.err.println("Device not initialized,use factory for device creation ");

        return false;
    }
    public boolean turnDeviceIdle(Device device){
        for (Device device1 : devicesInControll) {
            if(device1==device){
                if(device1.isInState==DeviceState.Broken){
                    System.err.println("Device is BROKEN and cannot be interacted with until fixed ");
                     return false;
               
                }
                if(device1.isInState!=DeviceState.On){
                    System.err.println("Device can be turned to idle only if it's On ");
                    return false;
                
                }
                
                device1.isInState=DeviceState.Idle;
                return true;
            }
        }
            System.err.println("Device not initialized,use factory for device creation ");

        return false;
    }
    public boolean insertItemIntoDevice(Device device,Item item){
        for(Device a: devicesInControll){
            if(device==a){
                if(item.freeStanding){
                    System.err.println("This item is freestanding and cannot be inserted into any device");
                    return false;
                }
                a.contains.add(item);
                item.containedIn=a;
                return true ;
            
            }
        
        }
        System.err.println("Device not initialized,use factory for device creation ");
        return false;
    }
    public boolean personUseDevice(Device device,Person person){
                if(device.usedBy==null){
                
                    device.usedBy=person;
                    return true;
                
                }
        return false;
        
    
    }
    public void stopUsingDevice(Device device){
        device.usedBy=null;
    
    }
}
