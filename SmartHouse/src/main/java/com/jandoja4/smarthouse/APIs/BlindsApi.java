/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jandoja4.smarthouse.APIs;

import com.jandoja4.smarthouse.Blinds;
import com.jandoja4.smarthouse.Window;
import java.util.ArrayList;

/**
 *
 * @author jarda
 */
public class BlindsApi {
    ArrayList<Blinds> controlledBlinds=new ArrayList<>();
    
    
    public void initializeBlinds(Blinds blinds){
        controlledBlinds.add(blinds);
    
    
    }
    public void initializeBlinds(Blinds[] blinds){
        for(Blinds a:blinds){
            controlledBlinds.add(a);
        
        
        }
    
    
    }
    public boolean closeBlinds(Blinds blinds){
        for(Blinds a:controlledBlinds){
            if(a==blinds){
                a.openedToPercentage=0;
                return true;
            
            }
        
        }
    return false;
    }
    public boolean openBlinds(Blinds blinds){
          for(Blinds a:controlledBlinds){
            if(a==blinds){
                a.openedToPercentage=100;
                return true;
            
            }
        
        }
    return false;
    }
    public boolean setBlindstoPercentage(Blinds blinds,int percentage){
         for(Blinds a:controlledBlinds){
            if(a==blinds){
                a.openedToPercentage=percentage;
                return true;
            
            }
        
        }
    return false ;
    
    
    }
   
    public boolean closeBlinds(Window window){
        if(window.coveredBy!=null){
            window.coveredBy.openedToPercentage=0;
            return true;}
        return false;
    
    }
    public boolean openBlinds(Window window){
        if(window.coveredBy!=null){
                window.coveredBy.openedToPercentage=100;
                return true;}
            return false;
    
    }
    public boolean setBlindstoPercentage(Window window,int percentage){
        if(window.coveredBy!=null){
            window.coveredBy.openedToPercentage=percentage;
            return true;}
        return false;
    
    
    }
    public void closeAllBlinds(){
        for(Blinds a:controlledBlinds){
            a.openedToPercentage=0;
        
        }
    
    }
      public void openAllBlinds(){
        for(Blinds a:controlledBlinds){
            a.openedToPercentage=100;
        
        }
    
    }
}
