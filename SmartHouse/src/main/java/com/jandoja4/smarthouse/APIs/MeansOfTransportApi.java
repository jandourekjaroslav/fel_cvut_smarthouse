/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jandoja4.smarthouse.APIs;

import com.jandoja4.smarthouse.MeansOfTransport;
import com.jandoja4.smarthouse.Person;
import java.util.ArrayList;

/**
 *
 * @author jarda
 */
public class MeansOfTransportApi {
    ArrayList<MeansOfTransport>controlledMoT=new ArrayList<>();
    
    
    public void initializeMoT(MeansOfTransport MoT){
        controlledMoT.add(MoT);
        
    
    }
    public MeansOfTransport[] findMeansOfTransports(String designation){
        ArrayList<MeansOfTransport> newList=new ArrayList<>();
        for(MeansOfTransport a:controlledMoT){
            if(a.designation.contains(designation)){
                newList.add(a);
                
            
            
            }
        
        
        }
     
      return (MeansOfTransport[]) newList.toArray();
    }
    public boolean useMoT(Person person,MeansOfTransport moT){
        if(moT.usedBy==null){
            moT.usedBy=person   ;
            return true;
        
        }
        return false;
    
    }
    public void stopUsingMoT(MeansOfTransport mot){
        mot.usedBy=null;
    
    }
}
