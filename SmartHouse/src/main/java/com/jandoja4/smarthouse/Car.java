package com.jandoja4.smarthouse;



public class Car extends MeansOfTransport {

  public Integer milage;

  public Integer fuel;

    public Car(Integer manufacturedIn, String condition, String designation, Integer milage,Integer fuelInTank) {
        super(manufacturedIn, condition, designation);
        this.milage=milage;
        this.fuel=fuelInTank;
    }

}