package com.jandoja4.smarthouse;

import java.util.ArrayList;


public class House {

  public String address;

  public String city;

  private String postCode;

    /**
   * 
   * @element-type Being
   */
  public ArrayList<Being>  inhabitedBy;
    /**
   * 
   * @element-type Floor
   */
  public ArrayList<Floor>  madeUpOfFloors;
  
  public House(String city,String address,String postCode){
      this.address=address;
      this.city=city;
      this.postCode=postCode;
      
  
  
  }
 
}