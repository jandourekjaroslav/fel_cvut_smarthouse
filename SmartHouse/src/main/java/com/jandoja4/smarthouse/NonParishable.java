package com.jandoja4.smarthouse;

public class NonParishable extends Item {

    public NonParishable(String itemDescription, boolean freeStanding) {
        super(itemDescription, freeStanding);
    }
}