/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jandoja4.smarthouse;

import com.jandoja4.smarthouse.enums.SpecialUtilityDeviceType;

/**
 *
 * @author jarda
 */
public class SpecialUtilityDevice extends Device{
    public SpecialUtilityDeviceType deviceType;

    public SpecialUtilityDevice( SpecialUtilityDeviceType deviceType,String nameOfDevice, Integer idleEnergyConsumption, Integer onEnergyConsumption, Integer offEnergyConsumption, Room InRoom, Integer onWaterConsumption) {
        super(nameOfDevice, idleEnergyConsumption, onEnergyConsumption, offEnergyConsumption, InRoom, onWaterConsumption);
         this.deviceType=deviceType;
    }
    
    
    }
    

