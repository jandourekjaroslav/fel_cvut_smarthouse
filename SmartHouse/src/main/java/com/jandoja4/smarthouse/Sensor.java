package com.jandoja4.smarthouse;

import com.jandoja4.smarthouse.enums.SensorType;

public class Sensor {

  public String unitsOfMeasure;

  public SensorType typeOfMeasurement;

  public Integer measuredValue;
  public Integer criticalLowValue;
  public Integer criticalHighValue;
  

    public Sensor(String unitsOfMeasure, SensorType typeOfMeasurement,Integer criticalLowValue,Integer criticalHighValue) {
        this.unitsOfMeasure = unitsOfMeasure;
        this.typeOfMeasurement = typeOfMeasurement;
        this.criticalHighValue=criticalHighValue;
        this.criticalLowValue=criticalLowValue;
    }

    public Room inRoom;
    public void Measure(){
        //random genneration no sensor actually exists
        Double a=(Math.floor(Math.random()*10));
        measuredValue=a.intValue();
    
    
    
    }

}