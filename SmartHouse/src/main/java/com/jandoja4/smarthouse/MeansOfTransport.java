package com.jandoja4.smarthouse;

public class MeansOfTransport {

  public Integer manufacturedIn;

  public String condition;
  public String designation;

  public Person usedBy;

    public MeansOfTransport(Integer manufacturedIn, String condition, String designation) {
        this.manufacturedIn = manufacturedIn;
        this.condition = condition;
        this.designation = designation;
        
    }

}