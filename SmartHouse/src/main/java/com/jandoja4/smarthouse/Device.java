package com.jandoja4.smarthouse;

import com.jandoja4.smarthouse.enums.DeviceState;
import java.util.ArrayList;


public class Device {
  
  public Integer deviceId;  
  public String nameOfDevice;
  public Integer idleEnergyConsumption;

  public Integer onEnergyConsumption;

  public Integer offEnergyConsumption;

  private Integer onWaterConsumption;
  public String userManual;
  private Integer functionalityPercentage;
  public Room InRoom;
  public DeviceState isInState;
  private DeviceObserver deviceObserver;
  public Person usedBy;
    /**
   * 
   * @element-type Item
   */
  public ArrayList<Item> contains;
  

    public Device(String nameOfDevice, Integer idleEnergyConsumption, Integer onEnergyConsumption, Integer offEnergyConsumption, Room InRoom,Integer onWaterConsumption) {
        this.nameOfDevice = nameOfDevice;
        this.idleEnergyConsumption = idleEnergyConsumption;
        this.onEnergyConsumption = onEnergyConsumption;
        this.offEnergyConsumption = offEnergyConsumption;
        this.functionalityPercentage=100;
        this.InRoom = InRoom;
    }
   

}