package com.jandoja4.smarthouse;

public class Pet extends Being {

  public String species;
  public Pet(String name,int age,String gender,String species,House house){
      this.age=age;
      this.name=name;
      this.gender=gender;
      this.species=species;
      this.inhabiting=house;
  
  }

}