/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jandoja4.smarthouse;

import com.jandoja4.smarthouse.APIs.DeviceApi;
import com.jandoja4.smarthouse.Factories.BeeingFactory;
import com.jandoja4.smarthouse.Factories.DeviceFactory;
import com.jandoja4.smarthouse.Factories.HouseFactory;
import java.util.ArrayList;

/**
 *
 * @author jarda
 */
public class StartConfig {
    
    
    public static void main(){
        HouseFactory FirstHouseF=new HouseFactory();
        House firstHouse=FirstHouseF.createHouse("Praha", "ALskdsa 121/2", "14000");     
        Floor ground=FirstHouseF.addFloor(firstHouse, 0);
        Floor firstFloor=FirstHouseF.addFloor(firstHouse, 1);
        Floor secondFloor=FirstHouseF.addFloor(firstHouse, 2);
        ArrayList<Room> rooms=new ArrayList<>();
        rooms.add(FirstHouseF.addRoom(ground, 20, "Hall"));
        rooms.add(FirstHouseF.addRoom(ground, 20, "LivingRoom"));
        rooms.add(FirstHouseF.addRoom(ground, 20, "Kitchen"));
        rooms.add(FirstHouseF.addRoom(ground, 20, "Toilet"));
        rooms.add(FirstHouseF.addRoom(ground, 20, "DinninRoom"));
        rooms.add(FirstHouseF.addRoom(firstFloor, 20, "ParentsBedroom"));
        rooms.add(FirstHouseF.addRoom(firstFloor, 20, "KidsBedRoom"));
        rooms.add(FirstHouseF.addRoom(firstFloor, 20, "GrandparentsBedroom"));
        rooms.add(FirstHouseF.addRoom(secondFloor, 20, "Attic"));
        for(Room a:rooms){
                FirstHouseF.addBlinds(FirstHouseF.addWindow(a));
        
        
        }
        BeeingFactory beeingFact=new BeeingFactory(firstHouse);
        ArrayList<Being> beings=new ArrayList<>();
        
        beings.add(beeingFact.addPerson(Role.Child, "ChungusChunly", 5, "NON BINARY HELICOPTER DON'T YOU DERE TO PRESUME MY GENDER"));
        beings.add(beeingFact.addPerson(Role.Child, "ChodeSmurly", 12, "Eunuch"));
        beings.add(beeingFact.addPerson(Role.Parent, "Beta", 35, "BallsInViceMale"));
        beings.add(beeingFact.addPerson(Role.HeadOfFamily, "FemdomMcAlpha", 32, "Strong independent woman who needs no maaaeean"));
        beings.add(beeingFact.addPerson(Role.GrandParent, "AshamemusCryAlot", 5, "Go'ol man"));
        beings.add(beeingFact.addPerson(Role.GrandParent, "LeMamaDeBeta", 5, "RetiredHousewife without personality"));
        beings.add(beeingFact.addPet("StandoffishPoS", 2, "Castrati Male", "Cat"))  ;  
        beings.add(beeingFact.addPet("SoonToDieHamster", 6, "WhoCares", "Hamster"));
        beings.add(beeingFact.addPet("ShiteGoldFish", 1, "meh", "CatFood"));
        
        DeviceFactory deviceFact=new DeviceFactory();
        DeviceApi deviceApi=deviceFact.createDevice(rooms.get(1), 55, 80,30 , "televize", 0);
        deviceFact.createDevice(rooms.get(1), 55, 80,30 , "Stolní Počítač", 0);
        deviceFact.createDevice(rooms.get(1), 55, 80,30 , "Akvírium", 60);
        deviceFact.createDevice(rooms.get(1), 55, 80,30 , "HifiSystém", 0);
        deviceFact.createDevice(rooms.get(1), 55, 80,30 , "Notebook", 0);
        deviceFact.createDevice(rooms.get(1), 55, 80,30 , "televize", 0);
        deviceFact.createDevice(rooms.get(1), 55, 80,30 , "televize", 0);
    
    }
}
