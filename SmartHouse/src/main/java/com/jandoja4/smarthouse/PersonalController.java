
package com.jandoja4.smarthouse;

import com.jandoja4.smarthouse.APIs.DeviceApi;
import com.jandoja4.smarthouse.APIs.BlindsApi;
import com.jandoja4.smarthouse.APIs.ItemApi;
import com.jandoja4.smarthouse.APIs.MeansOfTransportApi;
import com.jandoja4.smarthouse.APIs.SensorApi;

/**
 *
 * @author jarda
 */
public class PersonalController {
    House controllerForHouse;
    DeviceApi deviceApi;
    BlindsApi blindsApi;
    SensorApi sensorApi;
    ItemApi itemApi;
    MeansOfTransportApi motApi;
    Person Owner;
    Item usingItem;
    Device usingDevice;
    MeansOfTransport usingMoT;        

    public PersonalController(House controllerForHouse, DeviceApi deviceApi, BlindsApi blindsApi, SensorApi sensorApi, ItemApi itemApi, Person Owner,MeansOfTransportApi motApi) {
        this.controllerForHouse = controllerForHouse;
        this.deviceApi = deviceApi;
        this.blindsApi = blindsApi;
        this.sensorApi = sensorApi;
        this.sensorApi.initializeSensorApi(deviceApi, blindsApi);
        this.itemApi = itemApi;
        this.Owner = Owner;
        this.motApi=motApi;
    }

   
    public void startUsingItem(String description){
        int i=0;
        boolean found=false;
        Item[] itemsAvailable=itemApi.findItemByDescription(description);
        while(!found){
            for(Item a:itemsAvailable){
                if(itemApi.personUseItem(Owner, a)){
                  found=true;
                  usingItem=a;
                  break;
                
                }
            
            }
            
        
        }
    
    
    }
    public void startUsingDevice(String description){
            
            boolean found=false;
             Device[] devicesAvailable=deviceApi.findDeviceByName(description);
            while(!found){
                for( Device a:devicesAvailable){
                    if(deviceApi.personUseDevice(a,Owner)){
                      found=true;
                      usingDevice=a;
                      break;

                    }

                }


            }
    
    }
    public void startUsingMoT(String description){
       
            boolean found=false;
             MeansOfTransport[] motAvailable=motApi.findMeansOfTransports(description);
            while(!found){
                for( MeansOfTransport a:motAvailable){
                    if(motApi.useMoT(Owner, a)){
                      found=true;
                      usingMoT=a;
                      break;

                    }

                }


            }
    }
    
    public void stopUsingMot(){
        motApi.stopUsingMoT(usingMoT);
    
    }
    public void stopUsingDevice(){
        deviceApi.stopUsingDevice(usingDevice);
    
    
    }
    public void stopUsingItem(){
        itemApi.stopUsingItem(usingItem);
    }
}
